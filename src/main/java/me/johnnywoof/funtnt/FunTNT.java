package me.johnnywoof.funtnt;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class FunTNT extends JavaPlugin implements Listener {

    private final Set<UUID> fallingBlocks = new HashSet<>();
    private final Map<UUID, Collection<Region>> regions = new HashMap<>();
    private final Map<UUID, Collection<BlockData>> destroyedBlocks = new HashMap<>();

    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    public void onDisable() {
        this.getServer().getWorlds().forEach(this::fixWorld);
    }

    private void fixWorld(World w) {

        if (!this.fallingBlocks.isEmpty()) {

            w.getEntitiesByClass(FallingBlock.class).stream()
                    .filter(e -> this.fallingBlocks.remove(e.getUniqueId()))
                    .forEach(Entity::remove);

        }

        if (this.destroyedBlocks.containsKey(w.getUID())) {

            //noinspection deprecation
            this.destroyedBlocks.get(w.getUID())
                    .forEach(blockData ->
                            w.getBlockAt(blockData.getX(), blockData.getY(), blockData.getZ())
                                    .setTypeIdAndData(blockData.getMaterial().getId(), blockData.getData(), false)
                    );

            this.destroyedBlocks.clear();

        }

        this.regions.remove(w.getUID());

    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onWorldUnload(WorldUnloadEvent event) {
        this.fixWorld(event.getWorld());
    }

    @EventHandler(ignoreCancelled = true)
    public void onPhysics(BlockPhysicsEvent event) {

        if (event.getBlock().getType() == Material.AIR && this.regions.containsKey(event.getBlock().getWorld().getUID())) {

            Block block = event.getBlock();

            if (this.regions.get(block.getWorld().getUID()).stream().anyMatch(region -> region.isBlockInRegion(block))) {
                event.setCancelled(true);
            }

        }

    }

    @EventHandler(ignoreCancelled = true)
    public void onBlockFall(EntityChangeBlockEvent event) {

        if (event.getEntityType() == EntityType.FALLING_BLOCK && this.fallingBlocks.remove(event.getEntity().getUniqueId())) {

            event.setCancelled(true);
            return;

        }

        if (event.getTo() == Material.AIR) {

            Block block = event.getBlock();

            if (this.regions.get(block.getWorld().getUID()).stream().anyMatch(region -> region.isBlockInRegion(block))) {
                event.setCancelled(true);
            }

        }

    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityExplosion(EntityExplodeEvent event) {
        this.processExplosion(event.getLocation(), event.blockList());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockExplosion(BlockExplodeEvent event) {
        this.processExplosion(event.getBlock().getLocation(), event.blockList());
    }

    private void processExplosion(Location groundZero, Collection<Block> blockList) {

        if (blockList.isEmpty())
            return;

        int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE, minZ = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE, maxZ = Integer.MIN_VALUE;

        World world = groundZero.getWorld();

        for (Block block : blockList) {

            if (block.getType() == Material.TNT)
                continue;

            if (block.getX() < minX)
                minX = block.getX();

            if (block.getX() > maxX)
                maxX = block.getX();

            if (block.getY() < minY)
                minY = block.getY();

            if (block.getY() > maxY)
                maxY = block.getY();

            if (block.getZ() < minZ)
                minZ = block.getZ();

            if (block.getZ() > maxZ)
                maxZ = block.getZ();

            //noinspection deprecation
            FallingBlock fallingBlock = block.getWorld().spawnFallingBlock(block.getLocation(), new MaterialData(block.getType(), block.getData()));

            fallingBlock.setDropItem(false);
            fallingBlock.setHurtEntities(false);

            ThreadLocalRandom random = ThreadLocalRandom.current();

            fallingBlock.setVelocity(
                    new Vector(
                            random.nextDouble(-0.5, 0.5),
                            random.nextDouble(0.3, 1.5),
                            random.nextDouble(-0.5, 0.5)
                    )
            );

            BlockData blockData = new BlockData(block);

            this.fallingBlocks.add(fallingBlock.getUniqueId());
            this.destroyedBlocks.computeIfAbsent(block.getWorld().getUID(), k -> new ArrayList<>()).add(blockData);

            this.getServer().getScheduler().runTaskLater(this, () -> {

                //noinspection deprecation
                world.getBlockAt(blockData.getX(), blockData.getY(), blockData.getZ()).setTypeIdAndData(blockData.getMaterial().getId(), blockData.getData(), false);

                this.destroyedBlocks.getOrDefault(world.getUID(), Collections.emptyList()).remove(blockData);

            }, 150 + random.nextInt(250));

            block.setType(Material.AIR, false);

        }

        blockList.clear();

        Region region = new Region(minX - 1, minY - 1, minZ - 1, maxX + 1, maxY + 1, maxZ + 1, groundZero.getWorld().getUID());

        if (this.regions.computeIfAbsent(world.getUID(), k -> new ArrayList<>()).add(region)) {

            this.getServer().getScheduler().runTaskLater(this, () -> {

                Collection<Region> regions = this.regions.get(world.getUID());

                if (regions != null) {

                    regions.remove(region);

                    if (regions.isEmpty())
                        this.regions.remove(world.getUID());

                }

            }, 410);

        }

    }

}
