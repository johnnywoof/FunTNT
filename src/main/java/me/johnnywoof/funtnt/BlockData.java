package me.johnnywoof.funtnt;

import org.bukkit.Material;
import org.bukkit.block.Block;

public class BlockData {

    private final int x, y, z;

    private final Material material;
    private final byte data;

    public BlockData(Block block) {

        this.x = block.getX();
        this.y = block.getY();
        this.z = block.getZ();

        this.material = block.getType();
        //noinspection deprecation
        this.data = block.getData();

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public Material getMaterial() {
        return material;
    }

    public byte getData() {
        return data;
    }

}
