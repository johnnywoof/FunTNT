package me.johnnywoof.funtnt;

import org.bukkit.block.Block;

import java.util.UUID;


public class Region {

    private int x, y, z, mx, my, mz;
    private UUID worldUUID;

    public Region(int x, int y, int z, int mx, int my, int mz, UUID worldUUID) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.mx = mx;
        this.my = my;
        this.mz = mz;
        this.worldUUID = worldUUID;
    }

    public boolean isBlockInRegion(Block b) {

        return b.getWorld().getUID() == this.worldUUID &&
                b.getX() >= this.x
                && b.getX() <= this.mx
                && b.getY() >= this.y
                && b.getY() <= this.my
                && b.getZ() >= this.z
                && b.getZ() <= this.mz;
    }

}
